public class Ingrediente{
	int idInsumo;
	int cantidad;
	public Ingrediente(int idInsumo,int cantidad){
		setIdInsumo(idInsumo);
		setCantidad(cantidad);		
	}
	public void setIdInsumo(int idInsumo){
		this.idInsumo=idInsumo;
	}
	public int getIdInsumo(){
		return idInsumo;
	}
	public void setCantidad(int cantidad){
		this.cantidad=cantidad;
	}
	public int getCantidad(){
		return cantidad;
	}
	
}