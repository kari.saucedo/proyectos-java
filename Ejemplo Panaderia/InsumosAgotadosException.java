public class InsumosAgotadosException extends Exception { 
    public InsumosAgotadosException(String errorMessage) {
        super(errorMessage);
    }
}