public class TestPanaderia{
	public static void main(String[]args)throws InsumosAgotadosException{
		Panaderia panaderia=new Panaderia();
		Insumo[]insumos=panaderia.surtirInsumos();
		Pan[]panes=panaderia.obtenerRecetasPan();
		int cantidadInsumo1=insumos[0].getCantidad();
		int cantidadInsumo2=insumos[1].getCantidad();
		int cantidadInsumo3=insumos[2].getCantidad();
		while(cantidadInsumo1>0 && cantidadInsumo2>0 && cantidadInsumo3>0){
				panaderia.prepararPan1(insumos,panes);
				panaderia.prepararPan2(insumos,panes);
				panaderia.prepararPan3(insumos,panes);
				cantidadInsumo1=insumos[0].getCantidad();
				cantidadInsumo2=insumos[1].getCantidad();
				cantidadInsumo3=insumos[2].getCantidad();
				//System.out.println(cantidadInsumo1);
				//System.out.println(cantidadInsumo2);
				//System.out.println(cantidadInsumo3);
				if(cantidadInsumo1<=0 || cantidadInsumo2<=0 || cantidadInsumo3<=0)
					throw new InsumosAgotadosException("Uno o mas insumos se han agotado. No es posible preparar mas pan");			
		}
		
	}
}