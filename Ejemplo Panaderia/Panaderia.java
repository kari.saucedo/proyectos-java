public class Panaderia{
	public Insumo[] surtirInsumos(){
		Insumo[]insumos=new Insumo[3];
		insumos[0]=new Insumo(1,"Leche",30);
		insumos[1]=new Insumo(2,"Harina",45);
		insumos[2]=new Insumo(3,"Huevo",45);
		return insumos;
	}
	public Pan[]obtenerRecetasPan(){
	    Ingrediente[]ingredientesPan1=new Ingrediente[3];
		ingredientesPan1[0]=new Ingrediente(1,1);
		ingredientesPan1[1]=new Ingrediente(2,2);
		ingredientesPan1[2]=new Ingrediente(3,1);
		Pan pan1=new Pan(1,"Pan 1",ingredientesPan1);
		Ingrediente[]ingredientesPan2=new Ingrediente[3];
		ingredientesPan2[0]=new Ingrediente(1,2);
		ingredientesPan2[1]=new Ingrediente(2,3);
		ingredientesPan2[2]=new Ingrediente(3,4);
		Pan pan2=new Pan(2,"Pan 2",ingredientesPan2);
		Ingrediente[]ingredientesPan3=new Ingrediente[3];
		ingredientesPan3[0]=new Ingrediente(1,1);
		ingredientesPan3[1]=new Ingrediente(2,2);
		ingredientesPan3[2]=new Ingrediente(3,3);
		Pan pan3=new Pan(3,"Pan 3",ingredientesPan3);
		Pan[]panes=new Pan[3];
		panes[0]=pan1;
		panes[1]=pan2;
		panes[2]=pan3;
		return panes;	
	}
	public void prepararPan1(Insumo[]insumos,Pan[]panes){
		insumos[0].setCantidad(insumos[0].getCantidad()-panes[0].ingredientes[0].getCantidad());
		insumos[1].setCantidad(insumos[1].getCantidad()-panes[0].ingredientes[1].getCantidad());
		insumos[2].setCantidad(insumos[2].getCantidad()-panes[0].ingredientes[2].getCantidad());
	}
	public void prepararPan2(Insumo[]insumos,Pan[]panes){
		insumos[0].setCantidad(insumos[0].getCantidad()-panes[1].ingredientes[0].getCantidad());
		insumos[1].setCantidad(insumos[1].getCantidad()-panes[1].ingredientes[1].getCantidad());
		insumos[2].setCantidad(insumos[2].getCantidad()-panes[1].ingredientes[2].getCantidad());
	}
	public void prepararPan3(Insumo[]insumos,Pan[]panes){
		insumos[0].setCantidad(insumos[0].getCantidad()-panes[2].ingredientes[0].getCantidad());
		insumos[1].setCantidad(insumos[1].getCantidad()-panes[2].ingredientes[1].getCantidad());
		insumos[2].setCantidad(insumos[2].getCantidad()-panes[2].ingredientes[2].getCantidad());
		
	}	
}