public class Insumo{
	int id;
	String nombre;
	int cantidad;
	public Insumo(int id, String nombre, int cantidad){
		setId(id);
		setNombre(nombre);
		setCantidad(cantidad);
	}
	public void setId(int id){
		this.id=id;
	}
	public int getId(){
		return id;
	}
	public void setNombre(String nombre){
		this.nombre=nombre;
	}
	public String getNombre(){
		return nombre;
	}
	public void setCantidad(int cantidad){
		this.cantidad=cantidad;
	}
	public int getCantidad(){
		return cantidad;
	}
}