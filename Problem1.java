import java.util.Arrays;
public class Problem1{
	public static boolean isWordGuessed(String secretWord,Character[]lettersGuessed){
		int count=0;
		for(int i=0;i<secretWord.length();i++){
			if(Arrays.asList(lettersGuessed).contains(secretWord.charAt(i)))
				count++;
			else
				return false;
			if(count==secretWord.length())
				return true;
			else
				return false;
		}
		return false;
	}
	public static void main(String[]args){
		String secretWord="apple";
		Character[] lettersGuessed={'e','i','k','p','r','s'};
		System.out.println(isWordGuessed(secretWord,lettersGuessed));
	}
}
		