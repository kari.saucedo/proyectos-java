import java.util.Scanner;
public class Palindrome{
	public static void main(String[]args){
		String str="";
		System.out.println("Input a string: ");
		Scanner sc=new Scanner(System.in);
		str=sc.nextLine();
		str=str.toLowerCase();
		str=str.replaceAll("\\s", ""); 
		StringBuilder strReverse=new StringBuilder();
		strReverse.append(str);
		strReverse=strReverse.reverse();
		String reverse=strReverse.toString();
		reverse=reverse.replaceAll("\\s","");
		if(str.equals(reverse) && !(str.equals("")))
			System.out.println("Palindrome");
		else
			System.out.println("Not a palindrome");
	}
}