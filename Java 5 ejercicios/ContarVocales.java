import java.util.Scanner;
public class ContarVocales{
	public static void main(String[]args){
		String cadena="";
		int aMayus=0;
		int aMinus=0;
		int eMayus=0;
		int eMinus=0;
		int iMayus=0;
		int iMinus=0;
		int oMayus=0;
		int oMinus=0;
		int uMayus=0;
		int uMinus=0;
		Scanner sc=new Scanner(System.in);
		System.out.println("Introduzca una cadena:");
		cadena=sc.nextLine();
		for(int i=0;i<cadena.length();i++){
			char caracter=cadena.charAt(i);
			switch(caracter){
				case 'A':
					aMayus++;
				break;
				case 'a':
					aMinus++;
				break;
				case 'E':
					eMayus++;
				break;
				case 'e':
					eMinus++;
				break;
				case 'I':
					iMayus++;
				break;
				case 'i':
					iMinus++;
				break;
				case 'O':
					oMayus++;
				break;
				case 'o':
					oMinus++;
				break;
				case 'U':
					uMayus++;
				break;
				case 'u':
					uMinus++;
				break;
			}
		}
		System.out.println("Cantidad de A: "+aMayus);
		System.out.println("Cantidad de a: "+aMinus);
		System.out.println("Cantidad de E: "+eMayus);
		System.out.println("Cantidad de e: "+eMinus);
		System.out.println("Cantidad de I: "+iMayus);
		System.out.println("Cantidad de i: "+iMinus);
		System.out.println("Cantidad de O: "+oMayus);
		System.out.println("Cantidad de o: "+oMinus);
		System.out.println("Cantidad de U: "+uMayus);
		System.out.println("Cantidad de u: "+uMinus);
	}
}