import java.util.Arrays;
public class InsertionSort{
	public static void main(String[]args){
		int[]array=new int[]{5,6,4,12,13,23,20,40,9,11,10,28,38,34,30,25};
		int n = array.length;  
		for (int j = 1; j < n; j++) {  
			int key = array[j];  
            int i = j-1;  
            while ( (i > -1) && ( array [i] > key ) ) {  
                array [i+1] = array [i];  
                i--;  
            }  
            array[i+1] = key;  
        }  		
		System.out.println("Sorted array: "+Arrays.toString(array));
	}
}