public class DecimalToBinaryConverter{
	public static void main(String[]args){
		int n=45;
		int decimalNumber=n;
		int remainder=0;
		String binaryString="";
		while(n>0){
			remainder=n%2;
			binaryString=binaryString+Integer.toString(remainder);
			n=n/2;
		}
		binaryString = new StringBuffer(binaryString).reverse().toString();
		System.out.println(decimalNumber+" in binary equals: "+binaryString);
	}
}