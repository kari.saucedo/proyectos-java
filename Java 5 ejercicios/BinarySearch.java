public class BinarySearch{
	public static void main(String[]args){
		int[]array=new int[]{2,4,5,7,10,12,13,16,18,20,23,25,28,30,34,38,40};
		int target=34;
		int n=array.length;
		int min=1;
		int max=n-1;
		int guess=(max+min)/2;
		for(int i=0;i<n;i++){
			guess=(max+min)/2;
			if(array[guess]==target){
				System.out.println("Target "+target+" is at position "+guess);
				break;
			}else
				if(array[guess]<target)
					min=guess+1;
				else
					max=guess-1;				
		}		
	}
}