import java.util.Arrays;
public class Problem3{
	public static String getAvailableLetters(Character[]lettersGuessed){
		String letterList="abcdefghijklmnopqrstuvwxyz";
		String newList="";
		for(int i=0;i<letterList.length();i++){
			if(!(Arrays.asList(lettersGuessed).contains(letterList.charAt(i))))
				newList+=""+letterList.charAt(i);
			else
				newList+="";
		}
		return newList;
	}
	public static void main(String[]args){
		Character[] lettersGuessed={'e','i','k','p','r','s'};
		System.out.println(getAvailableLetters(lettersGuessed));
	}
}