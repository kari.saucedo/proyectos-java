import java.util.Arrays;
public class Problem2{
	public static String getGuessedWord(String secretWord, Character[]lettersGuessed){
		String newWord="";
		for(int i=0;i<secretWord.length();i++){
			if(Arrays.asList(lettersGuessed).contains(secretWord.charAt(i))){
				newWord+=""+secretWord.charAt(i);
			}
			else
				newWord+=" _ ";
		}
		return newWord;
	}
	public static void main(String[]args){
		String secretWord="apple";
		Character[] lettersGuessed={'e','i','k','p','r','s'};
		System.out.println(getGuessedWord(secretWord,lettersGuessed));
	}
}