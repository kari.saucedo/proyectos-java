public class Problem3{
	public static void main(String[]args){
		float balance = 89543.0f; 
		float annualInterestRate = 0.18f;
		float montIntRate=(annualInterestRate/12);
		float paymentLower=balance/12;
		float paymentUpper=(float)(balance*(Math.pow((1+montIntRate),12))/12);
		float origBalance=balance;
		float thePayment=0.0f;
		while(Math.abs(balance)!=0){
			thePayment=(paymentLower+paymentUpper)/2;
			balance=origBalance;
			for(int i=1;i<13;i++){
				balance=(balance-thePayment)*(1+montIntRate);
			}
			if(balance>0)
				paymentLower=thePayment;
			if(balance<0)
				paymentUpper=thePayment;
			balance=Math.round(balance*10000)/100;
		}
		//Round two places
		System.out.println(thePayment);
	}
}