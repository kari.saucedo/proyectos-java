public class Problem1{
	public static void main(String[]args){
		float balance=484;
		float annualInterestRate=0.2f;
		float monthlyPaymentRate=0.04f;
		System.out.println("Remaining balance: "+creditCardBalance(balance,annualInterestRate,monthlyPaymentRate));
	
	}
	public static float creditCardBalance(float balance,float anualInterestRate,float monthlyPaymentRate){
		float monthlyInterestRate=anualInterestRate/12;
		float totalPay=0;
		float minMonPay=0;
		for(int i=0;i<12;i++){
			minMonPay=balance*monthlyPaymentRate;
			totalPay+=minMonPay;
			balance=balance-minMonPay;
			balance=balance*(1+monthlyInterestRate);
		}
		//Round two decimal places
		return balance;
	}
}
		