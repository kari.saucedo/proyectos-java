public class Problem2{
	public static void main(String[]args){
		float balance = 3926; 
		float annualInterestRate = 0.2f;
		float minimumFixedPayment = 0.0f;
		float myBalance=balance;
		float monthlyInterestRate=annualInterestRate/12;
		while(myBalance>0){
			minimumFixedPayment+=10;
			for(int month=0;month<12;month++){
				myBalance-=minimumFixedPayment;
				myBalance+=(myBalance*monthlyInterestRate);
			}
			if(myBalance>0)
				myBalance=balance;
		}
		System.out.println(minimumFixedPayment);
	}
}